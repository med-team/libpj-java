//******************************************************************************
//
// File:    Test04.java
// Package: edu.rit.pj.cluster.test
// Unit:    Class edu.rit.pj.cluster.test.Test04
//
// This Java source file is copyright (C) 2008 by Alan Kaminsky. All rights
// reserved. For further information, contact the author, Alan Kaminsky, at
// ark@cs.rit.edu.
//
// This Java source file is part of the Parallel Java Library ("PJ"). PJ is free
// software; you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// PJ is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// Linking this library statically or dynamically with other modules is making a
// combined work based on this library. Thus, the terms and conditions of the
// GNU General Public License cover the whole combination.
//
// As a special exception, the copyright holders of this library give you
// permission to link this library with independent modules to produce an
// executable, regardless of the license terms of these independent modules, and
// to copy and distribute the resulting executable under terms of your choice,
// provided that you also meet, for each linked independent module, the terms
// and conditions of the license of that module. An independent module is a
// module which is not derived from or based on this library. If you modify this
// library, you may extend this exception to your version of the library, but
// you are not obligated to do so. If you do not wish to do so, delete this
// exception statement from your version.
//
// A copy of the GNU General Public License is provided in the file gpl.txt. You
// may also obtain a copy of the GNU General Public License on the World Wide
// Web at http://www.gnu.org/licenses/gpl.html.
//
//******************************************************************************

package edu.rit.pj.cluster.test;

import edu.rit.pj.Comm;

import edu.rit.util.Random;

/**
 * Class Test04 is a unit test main program for class {@linkplain
 * edu.rit.pj.Comm}. Each process waits a random number of seconds, then prints
 * a message.
 * <P>
 * Usage: java -Dpj.np=<I>K</I> edu.rit.pj.cluster.test.Test04 [ <I>lb</I> [
 * <I>ub</I> ] ]
 * <BR><I>lb</I> = Lower bound wait time (sec, default 1)
 * <BR><I>ub</I> = Upper bound wait time (sec, default <I>lb</I>+9)
 *
 * @author  Alan Kaminsky
 * @version 23-Apr-2008
 */
public class Test04
	{

// Prevent construction.

	private Test04()
		{
		}

// Main program.

	/**
	 * Main program.
	 */
	public static void main
		(String[] args)
		throws Exception
		{
		Comm.init (args);
		Comm world = Comm.world();
		int size = world.size();
		int rank = world.rank();
		int lb = args.length >= 1 ? Integer.parseInt (args[0]) : 1;
		int ub = args.length >= 2 ? Integer.parseInt (args[1]) : lb+9;
		Random prng = Random.getInstance (System.currentTimeMillis());
		int sec = prng.nextInt (ub-lb+1) + lb;
		Thread.sleep (sec*1000L);
		System.out.println (rank + ": " + sec + " sec");
		}

	}
